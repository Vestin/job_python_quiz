import asyncio
from asyncio import Event


async def main():
    x: list = []
    go: Event = asyncio.Event()
    await asyncio.gather(
        summer(x, go), gen(x, go)
    )
    print(x)


async def gen(x: list, go: Event) -> None:
    for i in range(10):
        x.append(i)
    go.set()


async def summer(x: list, go: Event) -> None:
    await go.wait()
    added: int = sum(x)
    x.clear()
    x.append(added)


asyncio.run(main())
