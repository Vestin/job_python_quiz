import asyncio
import random
from collections import defaultdict


async def main():
    basket: list = []
    bowls: dict[str, int] = defaultdict(lambda: 0)
    condition: asyncio.Condition = asyncio.Condition()
    sorting_task: asyncio.Task = asyncio.create_task(fruit_sorter(basket, bowls, condition))
    await fruit_factory(basket, condition)
    sorting_task.cancel()
    print(len(basket), sum(bowls.values()))


async def fruit_factory(basket: list, condition: asyncio.Condition) -> None:
    target: int = random.randint(1, 10)
    fruit_options: tuple = tuple({'🍌', '🍉', '🍎', '🍓', '🍊', '🥝'})
    async with condition:
        for i in range(100):
            basket.append(random.choice(fruit_options))
            if i == target:
                condition.notify()
                await condition.wait()

                target += random.randint(1, 10)
        condition.notify()
        await condition.wait()


async def fruit_sorter(basket: list, bowls: dict[str, int], condition: asyncio.Condition) -> None:
    while True:
        async with condition:
            for fruit in basket:
                bowls[fruit] += 1
            basket.clear()
            condition.notify()
            await condition.wait()


asyncio.run(main())
