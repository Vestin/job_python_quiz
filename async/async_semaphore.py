import asyncio
import random
from itertools import islice


async def main():
    coroutine_count: int = 15
    counters: list[int] = [0] * coroutine_count
    semaphore: asyncio.Semaphore = asyncio.Semaphore(3)
    xplotter: asyncio.Task = asyncio.create_task(plot(counters))
    await asyncio.gather(
        *(spitter(semaphore, i, counters) for i in range(1, coroutine_count))
    )
    xplotter.cancel()


async def spitter(semaphore: asyncio.Semaphore, count: int, counters: list[int]) -> None:
    async with semaphore:
        print(f"Semaphore acquired by {count}!")
        for i in range(1, count+1):
            counters[count] += 1
            await asyncio.sleep(random.uniform(0.0001, 0.01))
        print(f"Semaphore freed by {count}!")


async def plot(counters: list[int]):
    while True:
        print([f"{val/i:.2%}|{val:03d}/{i:03d}" for (i, val) in islice(enumerate(counters), 1, None)])
        await asyncio.sleep(0.001)


asyncio.run(main())
