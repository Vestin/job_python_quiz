import asyncio
from asyncio import Task, Barrier, Lock
from random import random, choice

fruit_options: tuple = tuple({'🍌', '🍉', '🍎', '🍓', '🍊', '🥝'})


async def spawner(field: list):
    while True:
        await asyncio.sleep(random())
        chosen_fruit: str = choice(fruit_options)
        field.append(chosen_fruit)
        print(f"Field: {field}")


async def harvester(target_fruit: str, field: list, picking_lock: Lock, barrier: Barrier):
    counter: int = 0
    while counter < 3:
        await asyncio.sleep(random())
        async with picking_lock:
            if target_fruit in field:
                field.remove(target_fruit)
                counter += 1
                print(f"{target_fruit} picker at {counter}.")
    print(f"{target_fruit} picker done with harvest! Waiting for barrier!")
    await barrier.wait()
    print(f"{target_fruit} picker got through the barrier!")


async def main():
    field: list = []
    picking_lock: Lock = Lock()
    barrier: Barrier = Barrier(len(fruit_options))
    spawner_task: Task = asyncio.create_task(spawner(field))
    await asyncio.gather(
        *(harvester(fruit, field, picking_lock, barrier) for fruit in fruit_options)
    )
    spawner_task.cancel()
    print(f"Field after harvest: {field}")


asyncio.run(main())
