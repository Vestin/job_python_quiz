import asyncio
from asyncio import Event


async def main():
    n: list = ['X', 'X']
    a_event: Event = Event()
    x_event: Event = Event()
    done: Event = Event()
    await asyncio.gather(x(n, a_event, x_event, done), a(n, a_event, x_event, done))
    print(n)


async def a(n: list, a_event: Event, x_event: Event, done: Event) -> None:
    while len(n) < 10:
        a_event.clear()
        v = len(n)
        if not any(v % number == 0 for number in filter(lambda s: isinstance(s, int), n)):
            n.append(v)
        else:
            x_event.set()
            await a_event.wait()
    x_event.set()
    done.set()


async def x(n: list, a_event: Event, x_event: Event, done: Event) -> None:
    while not done.is_set():
        await x_event.wait()
        x_event.clear()
        n.append('X')
        a_event.set()


asyncio.run(main())