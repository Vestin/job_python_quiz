import asyncio
import random


class C:
    source: int = 100
    destination: int = 0
    progress: list = []


async def main():
    lock: asyncio.Lock = asyncio.Lock()
    print(C.source == 100, C.destination == 0)
    await asyncio.gather(
        *(x(lock) for _ in range(100))
    )
    print(C.source == 0, C.destination == 100)
    print(C.progress == sorted(C.progress, reverse=True))


async def x(lock: asyncio.Lock) -> None:
    async with lock:
        r: int = random.randint(1, 9)
        portion: int = r if r <= C.source else C.source
        new_source: int = C.source - portion
        new_destination: int = C.destination + portion
        await asyncio.sleep(random.uniform(0.0001, 0.01))
        C.source = new_source
        C.progress.append(new_source)
        await asyncio.sleep(random.uniform(0.0001, 0.01))
        C.destination = new_destination

asyncio.run(main())
