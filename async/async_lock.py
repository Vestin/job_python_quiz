import asyncio
import random


async def main():
    lock: asyncio.Lock = asyncio.Lock()
    stuff: list = []
    await asyncio.gather(
        x(stuff, lock, 'A'), x(stuff, lock, 'B'), x(stuff, lock, 'C'),
    )
    print(stuff)


async def x(stuff: list, lock: asyncio.Lock, letter: str) -> None:
    for i in range(1, 5):
        async with lock:
            stuff.append(letter)
            print(letter, end='')
        await asyncio.sleep(random.uniform(0.01, 0.1))

asyncio.run(main())
