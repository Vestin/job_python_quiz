import asyncio
from asyncio import BoundedSemaphore, Semaphore


async def main():
    regular: Semaphore = Semaphore(1)
    bounded: BoundedSemaphore = BoundedSemaphore(1)
    for sem in (regular, bounded):
        print(sem)
        sem.release()
        print(sem)

asyncio.run(main())
