# Q42 (11)
# def f():
#     x = ("a", "b")
#     for element in enumerate(x):
#         print(element)
#
#
# f()

# Q43 (18)
# def f():
#     x = ("a", "b", "c")
#     y = range(2)
#     for element in zip(x, y):
#         print(element)
#
#
# f()

# Q44 (3)
# def f():
#     x = ("a", "b", "c")
#     y = [0, 1]
#     for element in zip(y, x):
#         print(element)
#
#
# f()

# Q45 (20)
# def f():
#     d: dict[int, int] = map(lambda x: x+1, range(2))
#     print(d)
#
#
# f()

# Q46 (1)
# def f():
#     m = map(lambda x: x**2, range(2))
#     for element in m:
#         print(element)
#
#
# f()

# Q47 (30)
# def f():
#     print(3^2)
#
#
# f()

# Q48 (33)
# def f():
#     x = range(3)
#     f = filter(lambda v: v, x)
#     for element in f:
#         print(element)
#
#
# f()

# Q49 (53)
# def f():
#     x = range(4)
#     y = map(lambda i: i//2, filter(lambda j: j % 2 == 0, x))
#     for element in y:
#         print(element)
#
#
# f()

# Q50 (10)
# def f():
#     x = range(3)
#     i = 10
#     y = map(lambda i: i+1, map(lambda i: i+1, x))
#     for element in y:
#         print(element)
#
#
# f()

# Q51 (13)
# def f():
#     x = range(3)
#     i = 10
#     y = map(lambda q: i+1, x)
#     for element in y:
#         print(element)
#
#
# f()

# Q52 (43)
# def f():
#     x = [2, 1, 3]
#     sorted(reversed(x))
#     for element in x:
#         print(element)
#
#
# f()

# Q53 (45)
# def f():
#     lambs = []
#     for i in range(3):
#         lambs.append(lambda: i + 1)
#     for lamb in lambs:
#         print(lamb())
#
#
# f()
