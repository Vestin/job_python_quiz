# Q01 (31)
# def one():
#     print(x = 1)
#
#
# one()

# Q02 (14)
# def two():
#     print("x = 1")
#     print(x := 2)
#
#
# two()

# Q03 (37)
# def three():
#     print("x = 1")
#     print(x := 2)
#     print(x > 3)
#
#
# three()

# Q04 (23)
# def gnomes():
#     print(1)
#     ...
#     print(3)
#
#
# gnomes()

# Q05 (24)
# def abba():
#     a = 1
#     b = 2
#     a, b = b, a
#     print(a, b)
#
#
# abba()

# Q06 (4)
# def g():
#     print([x for x in (1, 2, 3)])
#
#
# g()

# Q07 (34)
# def h():
#     print(x for x in (1, 2, 3))
#
#
# h()

# Q08 (28)
# def f():
#     x = 10
#     y = 10
#     print(x == y)
#     print(x is y)
#
#     x = y
#     print(x == y)
#     print(x is y)
#
#
# f()

# Q09 (6)
# def f():
#     x = 10 ** 300
#     y = 10 ** 300
#     print(x == y)
#     print(x is y)
#
#     x = y
#     print(x == y)
#     print(x is y)
#
#
# f()

# Q10 (16)
# def f():
#     x = ("HELLO")
#     x.append("WORLD")
#     for element in x:
#         print(element)
#
#
# f()

# Q11 (38)
# def f():
#     x = ("HELLO",)
#     x.append("WORLD")
#     for element in x:
#         print(element)
#
#
# f()




# TODO: concurrency, taking-by-reference and scopes, True/False being 1 and 0, usage of None, itertools
