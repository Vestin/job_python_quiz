# Q64 (???)
# class P:
#     def __init__(self):
#         self._x = "X"
#
#     @property
#     def x(self):
#         return self._x
#
#
# p: P = P()
# print(p.x())

# Q65 (???)
# class P:
#     def __init__(self):
#         self._x = "X"
#
#     @property
#     def x(self):
#         return self._x
#
#
# p: P = P()
# print(p.x)

# Q66 (???)
# class P:
#     def __init__(self):
#         self._x = "X"
#
#     @property
#     def x(self):
#         return self._x
#
#
# p: P = P()
# print(p.x)
# p.x = "Y"
# print(p.x)


# Q67 (???)
# class P:
#     def __init__(self):
#         self._x = "X"
#
#     @property
#     def x(self):
#         return self._x
#
#     @property
#     def x(self, value):
#         self._x = value
#
#
# p: P = P()
# print(p.x)
# p.x = "Y"
# print(p.x)

# Q68 (???)
# class P:
#     def __init__(self):
#         self._x = "X"
#
#     @property
#     def x(self):
#         return self._x
#
#     @x.setter
#     def x(self, value):
#         self._x = value
#
#
# p: P = P()
# print(p.x)
# p.x = "Y"
# print(p.x)
