class X:
    def __iter__(self):
        for _ in range(3):
            yield 3

    def __getitem__(self, item):
        return 4

    def __eq__(self, other):
        return True

# Q21 (27)
# def f():
#     x = X()
#     for i in range(3):
#         print(x[i])
#
#
# f()

# Q22 (50)
# def f():
#     x = X()
#     for element in x:
#         print(element)
#
#
# f()

# Q23 (44)
# def f():
#     x = X()
#     y = X()
#     z = 5
#     print(x == y, x is y)
#     print(y == x, y is x)
#     print(x == z, x is z)
#     print(z == x, z is x)
#     print(y == z, y is z)
#     print(z == y, z is y)
#
#
# f()
