# Q54 (??)

# x = 1
#
#
# class C:
#     x = 2
#
#     def __init__(self):
#         x = 3
#
#
# o = C()
# print(o.x)

# Q55 (???)
# x = 1
#
#
# class C:
#     x = 2
#
#     def __init__(self):
#         self.x = C.x
#         C.x++
#
#
# for _ in range(3):
#     o = C()
#     print(C.x)

# Q56 (???)
# x = 1
#
#
# class C:
#     x = 2
#
#     def __init__(self):
#         print(self.x is C.x, end=" ")
#         self.x = C.x
#         print(self.x is C.x, end=" ")
#         C.x += 1
#         print(self.x is C.x, end=" ")
#
#
# for _ in range(3):
#     o = C()
#     print(o.x, end=" ")
#     print(o.x is C.x)

# Q57 (???)
# x = 1
#
#
# class C:
#     x = 2
#
#     def __init__(self):
#         x = 8
#         self.x = x
#
#
# o = C()
# print(x, C.x, o.x)

# Q58 (???)
# x = 1
#
#
# class C:
#     x = 2
#
#     def __init__(self):
#         global x
#         x = 8
#         self.x = x
#
#
# o = C()
# print(x, C.x, o.x)

# Q59 (???)
# class C:
#     def __init__(self):
#         C.x = 5
#         self.x = 6
#
#
# o = C()
# print(C.x, o.x)


# Q60 (???)
# class C:
#     def __init__(self):
#         C.x = 5
#         self.x = 6
#
#
# print(C.x, C().x)


# Q61 (???)
# class C:
#     C.x = 3
#     def __init__(self):
#         C.x = 5
#         self.x = 6
#
#
# print(C.x, C().x)


# Q62 (???)
# class C:
#     x = 0
#
#     def __init__(self):
#         self.x = 1
#
#     @classmethod
#     def gimme(self):
#         return self.x
#
#
# o = C()
# print(o.gimme())

# Q63 (???)
# class C:
#     x = 5
#
#     @staticmethod
#     def gimme():
#         return C.x
#
#
# print(C.gimme())
