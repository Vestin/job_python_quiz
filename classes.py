# .
class A:
    x = 0
    def __init__(self):
        print("A")


class B(A):
    def __init__(self):
        print("B")


class C(B, A):
    def __init__(self):
        print("C")


# Q24 (48)
# def f():
#     C()
#
#
# f()

# Q25 (26)
# def f():
#     m = C()
#     print(C.x, m.x, C.x == m.x, C.x is m.x)
#
#
# f()

# Q26 (49)
# def f():
#     x = C()
#     y = C()
#     print(x == y, x is y)
#
#
# f()

# Q27 (39)
# def f():
#     m = C()
#     m.x = 1
#     print(C.x, B.x, A.x, m.x)
#
#
# f()

# Q28 (21)
# def f():
#     m = C()
#     B.x = 1
#     print(C.x, B.x, A.x, m.x)
#
#
# f()

# Q29 (29)
# def f():
#     for i in C.mro():
#         print(i.__name__)
#
#
# f()
