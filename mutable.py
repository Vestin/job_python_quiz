# Q36 (35)
# def ff():
#     def f(s):
#         s[0] = "H"
#     x = "Cat"
#     print(f(x))
#
#
# ff()

# Q37 (51)
# def fg():
#     def f(s):
#         s[0] = "H"
#     x = [1, 2, 3]
#     f(x)
#     print(x)
#
#
# fg()

# Q38 (42)
# def fh():
#     def f(s):
#         s[0] = "H"
#     x = (1, 2, 3)
#     f(x)
#     print(x)
#
#
# fh()

# Q39 (41)
# def fi():
#     def f(s):
#         s[0] = "H"
#     x = {k: k+1 for k in range(1)}
#     f(x)
#     print(x)
#
#
# fi()

# Q40 (12)
# def fj():
#     def f(s):
#         s[0] = "H"
#     x = []
#     f(x)
#     print(x)
#
#
# fj()

# Q41 (17)
# def f():
#     def star(target: int = 0, arr: list = list(x for x in range(10))):
#         arr[target % len(arr)] = "*"
#         return arr
#     a = star()
#     print(a)
#     b = star(1)
#     print(b)
#     c = star(15)
#     print(c)
#
#
# f()
