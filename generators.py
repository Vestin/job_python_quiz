# Q12 (19)
# def f():
#     def g():
#         for x in range(3):
#             return x
#     for i in g():
#         print(i)
#
#
# f()

# Q13 (47)
# def f():
#     def g():
#         for x in range(3):
#             return x
#     print(g())
#
#
# f()

# Q14 (8)
# def f():
#     def g():
#         for x in range(3):
#             yield x
#     for i in g():
#         print(i)
#
#
# f()

# Q15 (2)
# def f():
#     def g():
#         for x in range(3):
#             yield x
#     print(g())
#
#
# f()

# Q16 (36)
# def f():
#     def g(x=[3,1,2,5,0]):
#         v = x.pop()
#         return v
#
#     for i in iter(g, 2):
#         print(i)
#
#
# f()

# Q17 (22)
# def f():
#     r = range(3)
#     print(next(r))
#
#
# f()

# Q18 (25)
# def f():
#     r = range(3)
#     print(iter(r))
#
#
# f()

# Q19 (40)
# def f():
#     r = range(3)
#     print(iter(next(r)))
#
#
# f()

# Q20 (5)
# def f():
#     r = range(3)
#     print(next(iter(r)))
#
#
# f()
