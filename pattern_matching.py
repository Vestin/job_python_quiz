# Q30 (46)
# def m():
#     for value in range(3):
#         match value:
#             case 1:
#                 print("B")
#                 break
#             case 2:
#                 print("U")
#             case 3:
#                 print("Z")
#                 break
#             case _:
#                 print("X")
#
#
# m()

# Q31 (7)
# def a():
#     n = 5
#     for value in range(3):
#         match value:
#             case 1:
#                 print("R")
#             case n if n == 0:
#                 print("X")
#             case _:
#                 print(n)
#
#
# a()

# Q32 (15)
# def t():
#     n = 5
#     match n:
#         case int():
#             print("N")
#         case 5:
#             print("I")
#         case _:
#             print("X")
#
#
# t()

# Q33 (52)
# def x():
#     v = (1, 2)
#     match v:
#         case (int(v), int()):
#             print("A")
#         case (1, 2):
#             print("B")
#         case _:
#             print("C")
#     print(v)
#
#
# x()

# Q34 (32)
# def b():
#     v = (1, 2, 3)
#     for _ in v:
#         match v:
#             case int():
#                 print("A")
#             case (int(v), int(), int()):
#                 print("B")
#             case (1, 2, 3):
#                 print("C")
#             case _:
#                 print("D")
#
#
# b()
