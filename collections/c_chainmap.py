from collections import ChainMap

# 1
a: dict = {n: m for n in range(10) for m in range(3)}
print(a)

# 2
b = {n: {n**m for m in range(3)} for n in range(10)}
print(b)

# 3
c: dict = {0: "a", 1: "z", 2: "x"}
d: dict = {0: "b", 1: "z", 2: 7}
c.update(d)
print(c)
print(d)

# 4
# from collections import ChainMap
e: dict = {chr(ord('a')+i): i for i in range(3)}
f: dict = {chr(ord('a')+i): chr(ord('a')+i+1) for i in range(5)}
cm: ChainMap = ChainMap(e, f)
print(f"e: {e}")
print(f"f: {f}")
print("ChainMap:")
for key, value in cm.items():
    print(f"{key}: {value}")

# 5
# from collections import ChainMap
g: dict = {chr(ord('a')+i): i for i in range(5)}
h: dict = {chr(ord('a')+i): chr(ord('a')+i+1) for i in range(3)}
cm: ChainMap = ChainMap(g, h)
print(f"g: {g}")
print(f"h: {h}")
print("ChainMap:")
for key, value in cm.items():
    print(f"{key}: {value}")

# 6
# from collections import ChainMap
k: dict = {chr(ord('a')+i): i for i in range(5)}
l: dict = {chr(ord('a')+i): chr(ord('a')+i+1) for i in range(3)}
cm: ChainMap = ChainMap(k, l)
print(f"k: {k}")
print(f"l: {l}")
print("ChainMap:")
del cm.maps[0]
for key, value in cm.items():
    print(f"{key}: {value}")
print(cm.maps[0] is l)

# 7
# from collections import ChainMap
m: dict = {i: i for i in range(3)}
cm_m: ChainMap = ChainMap(m)
n: dict = {0: 9}
cm_n: ChainMap = cm_m.new_child(n)
print(cm_m is cm_n)
for key in cm_m:
    print(f"{key}: {cm_m[key]} | {cm_n[key]}")
print(cm_n.parents == cm_m)
print(cm_n.parents == m)
print(cm_n.parents == n)
print(cm_n.parents == {})
