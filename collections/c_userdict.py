from collections import UserDict


class DoubleDict(UserDict):
    def __getitem__(self, item):
        if item in self.data:
            return self.data[item]
        if isinstance(item, int):
            return item * 2
        return item


dd = DoubleDict()
dd[2] = 10
print(dd[1], dd[2], dd["a"])
