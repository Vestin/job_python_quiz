from collections import defaultdict


def apple() -> str:
    return "🍎"


fruit: defaultdict = defaultdict(apple, {0: "🍌", None: "🍑"})
for key in [None, *(x for x in range(3))]:
    print(f"{str(key):^8} | {fruit[key] = }; {fruit.get(key) = }")

print("*".center(50, "~"))

for key in [None, *(x for x in range(5))]:
    print(f"{str(key):^8} | {fruit.get(key) = }; {fruit[key] = }")

print("*".center(50, "~"))

for key in [None, *(x for x in range(5))]:
    print(f"{str(key):^8} | {fruit.get(key) = }; {fruit[key] = }")
