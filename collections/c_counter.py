# 1
import random
from collections import Counter

fruit_options: tuple = tuple({'🍌', '🍉', '🍎', '🍓', '🍊', '🥝'})
basket_a: list = random.choices(fruit_options, k=15)
basket_a.extend(fruit_options[0] * 5)
basket_b: list = random.choices(fruit_options, k=15)
basket_b.extend(fruit_options[1] * 5)
fruit_counter_a: Counter = Counter(basket_a)
fruit_counter_b: Counter = Counter(basket_b)
print(fruit_counter_a, fruit_counter_b, sep='\n')
print(f"A+B = {fruit_counter_a + fruit_counter_b}")
print(f"A-B = {fruit_counter_a - fruit_counter_b}")
print(f"A&B = {fruit_counter_a & fruit_counter_b}")
print(f"A|B = {fruit_counter_a | fruit_counter_b}")
print(f"Most common A: {fruit_counter_a.most_common(1)[0]}, B: {fruit_counter_b.most_common(1)[0]}.")
print(f"Total fruit count A: {fruit_counter_a.total()}, B: {fruit_counter_b.total()}.")
