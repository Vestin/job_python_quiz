from collections import namedtuple

RGBA = namedtuple("RGBA", ("red", "green", "blue", "alpha"), defaults=(0, 0, 0, 255))
rgba: RGBA = RGBA(green=255)
print(rgba)
rgba_2 = RGBA._make((1, 2, 3, 4))
print(rgba_2)
