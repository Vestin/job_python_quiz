from collections import OrderedDict

od: OrderedDict = OrderedDict()
od[2] = "b"
od[1] = "a"
od[3] = "c"

while od:
    print(od)
    key, value = od.popitem(last=True)
    print(f"{key} -> {value}")
print(od)
