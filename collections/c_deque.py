import random
from collections import deque

# 1

revolver: deque[bool] = deque(maxlen=6)
for i in range(revolver.maxlen):
    revolver.append(False)
revolver.append(True)
revolver.rotate(random.randint(1, revolver.maxlen))
for chamber in revolver:
    if chamber:
        print("💥")
        break
    else:
        print("✔", end="")

# 2
queue_size: int = 10
queue: deque[int] = deque(maxlen=queue_size)
operations: list[bool] = []
while len(operations) < queue_size * 2:
    push_count: int = operations.count(True)
    pop_count: int = operations.count(False)
    if push_count == pop_count:
        operations.append(True)
    else:
        if push_count < queue_size:
            operations.append(random.choice((True, False)))
        else:
            operations.append(False)

counter: int = 1
for push in operations:
    if push:
        print(f"Pushing {counter}.")
        queue.append(counter)
        counter += 1
    else:
        element = queue.popleft()
        print(f"Popping {element}.")
    print(queue)

# 3


def h_print(left, center, right, round_number: int, step: int):
    max_width: int = max(max(tower) if tower else 0 for tower in (left, center, right))
    max_height: int = max(len(tower) for tower in (left, center, right))
    print(f"{round_number}:{step}".center(max_width * 3 + 2))
    print("|".join(label.center(max_width, "_") for label in "ABC"))
    for row in range(max_height):
        tower_string_list: list = []
        for tower in (left, center, right):
            width: int = tower[max_height - 1 - row] if len(tower) >= max_height - row else 0
            tower_string: str = f"{'*' * width:^{max_width}}"
            tower_string_list.append(tower_string)
        print("|".join(tower_string_list))
    print("~" * (max_width * 3 + 2))


def move_a_b_or_b_a(a: deque, b: deque):
    if a and (not b or b[-1] > a[-1]):
        b.append(a.pop())
    elif b and (not a or a[-1] > b[-1]):
        a.append(b.pop())


top_height: int = 5
left_tower: deque = deque(disk for disk in range(top_height, 0, -1))
center_tower: deque = deque()
right_tower: deque = deque()

h_round: int = 0
h_print(left_tower, center_tower, right_tower, h_round, 0)
while len(right_tower) < top_height:
    move_a_b_or_b_a(left_tower, center_tower)
    h_print(left_tower, center_tower, right_tower, h_round, 1)
    move_a_b_or_b_a(left_tower, right_tower)
    h_print(left_tower, center_tower, right_tower, h_round, 2)
    move_a_b_or_b_a(center_tower, right_tower)
    h_print(left_tower, center_tower, right_tower, h_round, 3)
    h_round += 1
