# https://docs.python.org/3/library/string.html#format-specification-mini-language

s: str = "Hello"
i: int = 43
f: float = 3.17

# 1
print(f"{s:<<10}|{i:^^10}|{f:>>10}")

# 2
print(f"{i:#b}|{i:#o}|{i:#x}|{i:#X}")

# 3
print(f"{s:{s[i^i]}^{i}}")

# 4
print(f"{i*i:❤<8_}")

# 5
# print(f"{f:d}")

# 6
print(f"{i:c}")

# 7
print(f"{i:.2f}")

# 8
print(f"{i*i*i:$<+#10_x}")

# 9
print(f"{s:🔥^8.4}")

# 10
print(f"{i//i:.0%}")

# 11
print(f"{'❤'!r} | {'❤'!s} | {'❤'!a}")
